jQuery( function( $ ) {
	$( '#woocommerce_wcnlpc_checkout_api_source' ).on('change', function (e) {
		var api_source = $(this).val();
		$(':input[data-api_source_specific]').each( function(){
			if (typeof api_source != 'undefined' && $(this).data('api_source_specific') == api_source ) {
				$(this).closest('tr').show();
			} else {
				$(this).closest('tr').hide();
			}
		});
	}).trigger('change');

	$( 'select#woocommerce_wcnlpc_field_visibility' ).on('change', function (e) {
		if ( $(this).val() == 'nl_plus' ) {
			$( this ).closest('tr').next().show();
		} else {
			$( this ).closest('tr').next().hide();
		}
	}).trigger('change');
	
	$( '#woocommerce_wcnlpc_api_test_connection' ).on( 'click', function( e ) {
		e.preventDefault();
		
		let $button     = $( this );
		let $spinner    = $button.closest( 'td' ).find( '.wpo-spinner' );
		let $notice     = $button.closest( 'td' ).find( '.notice' );
		let api_source  = $( '#woocommerce_wcnlpc_checkout_api_source' ).val();
		let $api_fields = {};
		
		$.each( wpo_wcnlpc.api_settings_fields, function( i, v ) {
			if ( i === api_source ) {
				$api_fields['key']    = v.key.length > 0 ? $( '#' + v.key ).val() : '';
				$api_fields['secret'] = v.secret.length > 0 ? $( '#' + v.secret ).val() : '';
				$api_fields['token']  = v.token.length > 0 ? $( '#' + v.token ).val() : '';
			}
		} );
		
		$notice.hide();
		$notice.removeClass('notice-success notice-error');
		$spinner.css( 'display', 'inline-block' );
		$button.attr( 'disabled', true );
		
		$.ajax( {
			url:     wpo_wcnlpc.ajaxurl,
			data: {
				action:          'wpo_wcnlpc_api_request',
				security:        wpo_wcnlpc.nonce,
				api_source:      api_source,
				postcode:        wpo_wcnlpc.test_postcode,
				house_number:    wpo_wcnlpc.test_postcode_number,
				test_connection: true,
				api_fields:      $api_fields,
			},
			type:    'POST',
			cache:   false,
			timeout: wpo_wcnlpc.xhr_timeout, // timeout, 8000ms by default
			success ( response ) {
				if ( response.success ) {
					$notice.addClass( 'notice-success' );
					$notice.find( 'p' ).text( wpo_wcnlpc.test_success );
				} else if ( response.data.message ) {
					$notice.addClass( 'notice-error' );
					$notice.find( 'p' ).text( response.data.message );
				} else {
					$notice.addClass( 'notice-error' );
					$notice.find( 'p' ).text( wpo_wcnlpc.test_error );
				}
				
				$notice.show();
				$spinner.css( 'display', 'none' );
				$button.attr( 'disabled', false );
			},
			error ( xhr, status, error ) {
				let error_message = xhr.status + ': ' + xhr.statusText;
				$notice.addClass( 'notice-error' );
				$notice.find( 'p' ).text( error_message );
				
				$notice.show();
				$spinner.css( 'display', 'none' );
				$button.attr( 'disabled', false );
			}
		} );
	} );
	
});