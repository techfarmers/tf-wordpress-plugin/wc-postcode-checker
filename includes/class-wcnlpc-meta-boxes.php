<?php
/**
 * Main plugin functions
 */

use WPO\WC\Postcode_Checker\WPO_WCNLPC_Order_Util;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

if ( ! class_exists( 'WPO_WCNLPC_Meta_Boxes' ) ) :

class WPO_WCNLPC_Meta_Boxes {
	
	public $order_util;
	
	function __construct()	{
		$this->order_util = WPO_WCNLPC_Order_Util::instance();

		add_action( 'add_meta_boxes', array( $this, 'add_meta_boxes' ), 10, 2 );
		$this->wcnlpc_actions();
	}

	public function add_meta_boxes( $post_type, $post ) {
		$screen = $this->order_util->custom_order_table_screen();

		if ( $this->order_util->custom_orders_table_usage_is_enabled() && $post_type === $screen ) {
			$post_id = $post->get_id();
		} else {
			$post_id = $post->ID;
		}

		if ( $this->order_util->get_order_type( $post_id ) === 'shop_order' ) {
			if ( ! empty( $order = wc_get_order( $post_id ) ) && ! empty( $order->get_meta( '_postcode_api_data' ) ) ) {
				add_meta_box(
					'wpo_wcnlpc_api_data',
					__( 'postcode.eu API data', 'wpo_wcnlpc' ),
					array( $this, 'show_api_data'),
					$screen,
					'normal',
					'default'
				);
			}
		}
	}

	public function show_api_data( $order_id ) {
		$order    = wc_get_order( $order_id );
		$api_data = $order->get_meta( '_postcode_api_data' );
		if ( empty( $api_data ) ) {
			return;
		}

		$remove_data_url = add_query_arg('wcnlpc_action', 'remove_data');
		printf('<a href="%s" class="button">%s</a>', esc_url( $remove_data_url ), __( 'Remove data', 'wpo_wcnlpc' ) );

		echo "<table><tr>";
		foreach ($api_data as $form => $data) {
			switch ($form) {
				case 'billing':
					$title = __('Billing Address', 'wpo_wcnlpc');
					break;
				case 'shipping':
					$title = __('Shipping Address', 'wpo_wcnlpc');
					break;
				default:
					$title = ucfirst($form);
					break;
			}

			echo "<td><h3>{$title}</h3>";
			echo "<table>";
			foreach ($data as $key => $value) {
				if (is_array($value)) {
					$value = implode(', ', $value);
				}
				$title = $key;
				if (empty($value)) {
					$value = '-';
				}
				echo "<tr><th style=\"text-align:left;\">{$title}</th><td>{$value}</td></tr>";
			}
			echo "</table></td>";
		}
		echo "</tr></table>";
	}

	public function wcnlpc_actions() {
		if ( ! isset( $_GET['wcnlpc_action'] ) ) {
			return;
		}

		switch ( $_GET['wcnlpc_action'] ) {
			case 'remove_data':
				$order_id = $_GET['post'];
				$order    = wc_get_order( $order_id );
				if ( ! empty( $order ) ) {
					$order->delete_meta_data( '_postcode_api_data' );
					$order->save_meta_data();	
				}
				break;
		}

		$action_done = remove_query_arg( 'wcnlpc_action' );
		wp_safe_redirect( esc_url_raw( $action_done ) );
	}
}

endif; // class_exists

return new WPO_WCNLPC_Meta_Boxes();