<?php
/**
 * Main plugin functions
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

if ( !class_exists( 'WPO_WCNLPC_Checkout' ) ) :

class WPO_WCNLPC_Checkout {
	
	function __construct()	{
		// check if enabled
		if ( WPO_WCNLPC()->validation_enabled() === false ) {
			return;
		}

		$address_field_priority = $this->get_checkout_fields_hook_priority();
		add_filter( 'woocommerce_billing_fields', array( $this, 'nl_billing_fields' ), $address_field_priority, 2 );
		add_filter( 'woocommerce_shipping_fields', array( $this, 'nl_shipping_fields' ), $address_field_priority, 2 );
		add_filter( 'woocommerce_get_country_locale', array( $this, 'unset_postcode_before_city_nl' ), 10, 1 );

		add_filter( 'wpo_wcnlpc_postcode_field_countries', array( $this, 'enable_fields_for_additional_countries' ) );

		$myparcel      = WPO_WCNLPC()->get_myparcel_version();
		$postnl        = WPO_WCNLPC()->get_postnl_version();
		$myparcel_fork = $myparcel ? $myparcel : $postnl; // PostNL is a MyParcel fork

		// add street, house number and house number suffix fields unless an old version of the MyParcel plugin is active
		if ( ! $myparcel_fork || version_compare( $myparcel_fork, '3.0.5', '>=' ) ) {
			include( 'class-wcnlpc-nlpostcode-fields.php' );
		}

		// MyParcel limits suffix to 4 characters
		if ( $myparcel_fork ) {
			add_filter( 'woocommerce_billing_fields', array( $this, 'myparcel_suffix_limit' ), 99, 2 );
			add_filter( 'woocommerce_shipping_fields', array( $this, 'myparcel_suffix_limit' ), 99, 2 );
		}

		// Klarna Payments compatibility
		add_filter ( 'wc_kp_remove_postcode_spaces', '__return_true' );

		// add_filter( 'woocommerce_checkout_process', array( $this, 'validate_postcode' ), 10, 1 );
		// remove duplicate street name
		add_filter('woocommerce_process_checkout_field_billing_street_name', array( $this, 'clean_billing_street_duplicates' ) );
		add_filter('woocommerce_process_checkout_field_shipping_street_name', array( $this, 'clean_shipping_street_duplicates' ) );

		add_filter( 'woocommerce_checkout_process', array( $this, 'supplement_missing_checkout_data' ), 999 );

		// convert address data from pre-postcode checker user profile data or fetch from session
		$this->init_default_checkout_values_fallbacK();

		// store values for street + number fields in session
		add_action('woocommerce_checkout_update_order_review', array( $this, 'set_session_field_data' ), 10, 1 );

		// clears session data when updating customer address
		add_action( 'woocommerce_customer_save_address', array( $this, 'unset_session_field_data' ), 10, 2 );

		// store api response in order
		// add_action( 'woocommerce_checkout_before_customer_details', array( $this, 'checkout_api_response_input' ), 10, 1 );
		// add_action( 'woocommerce_checkout_update_order_meta', array( $this, 'save_postcode_data' ), 10, 2 );
	}

	public function init_default_checkout_values_fallback() {
		$fields = array( 'street_name', 'house_number', 'house_number_suffix' );
		$forms  = array( 'billing', 'shipping' );
		foreach ( $fields as $field ) {
			foreach ( $forms as $form ) {
				// profile data fallback
				if ( get_option( 'woocommerce_wcnlpc_enable_profile_data_fallback' ) == 'yes' ) {
					add_filter( "default_checkout_{$form}_{$field}", array( $this, 'default_checkout_values_fallback' ), 10, 2 );
				}
				// session data
				add_filter( "default_checkout_{$form}_{$field}", array( $this, 'get_session_field_data' ), 10, 2 );
			}
		}
	}

	/**
	 * Store customer data for street + number fields in session
	 * 
	 * @param  string $data query string of posted data
	 * @return void
	 */
	public function set_session_field_data( $data ) {
		// parse query string data
		if ( is_string( $data ) ) {
			$posted = array();
			parse_str( $data, $posted );
		}
		if ( ! empty( $posted ) ) {
			$fields     = array( 'street_name', 'house_number', 'house_number_suffix' );
			$forms      = array( 'billing', 'shipping' );
			$field_data = array();
			foreach ( $forms as $form ) {
				foreach ( $fields as $field ) {
					$field_name = "{$form}_{$field}";
					$field_data[$field_name] = !empty( $posted[$field_name] ) ? $posted[$field_name] : '';
				}
			}
			
			WC()->session->set( 'wcnlpc_data', $field_data );
		}
	}

	/**
	 * Unset customer data for street + number fields in session
	 *
	 * @param int    $user_id User ID being saved.
	 * @param string $load_address Type of address e.g. billing or shipping.
	 * @return void
	 */
	public function unset_session_field_data( $user_id, $load_address ) {
		$woo_session = WC()->session;
		if ( empty( $woo_session ) ) {
			return;
		}

		$session_data = $woo_session->get( 'wcnlpc_data' );
		if ( ! empty( $session_data ) ) {
			foreach ( array( 'street_name', 'house_number', 'house_number_suffix' ) as $field ) {
				$form_field = $load_address . '_' . $field;
				if ( isset( $session_data[ $form_field ] ) ) {
					unset( $session_data[ $form_field ] );
				}
			}
			WC()->session->set( 'wcnlpc_data', $session_data );
		}
	}

	/**
	 * Retrieve customer data for street + number fields from the session
	 *
	 * @param string The default value.
	 * @param string $input Name of the input we want to grab data for. e.g. billing_country.
	 * @return string The default value.
	 */
	public function get_session_field_data( $value, $input ) {
		$fields = array( 'street_name', 'house_number', 'house_number_suffix' );
		if ( ! in_array( str_replace( array( 'billing_', 'shipping_' ), '', $input ), $fields ) ) {
			return $value;
		}

		$data = ! empty( WC()->session ) ? WC()->session->get( 'wcnlpc_data' ) : null;
		if ( ! empty( $data ) && ! empty( $data[$input] ) ) {
			$value = $data[$input];	
		}
		return $value;
	}

	public function default_checkout_values_fallback( $default_value, $input ) {
		if ( empty( $default_value ) && is_user_logged_in() ) {
			$field_name = str_replace( array( 'billing_', 'shipping_' ), '', $input );
			$form = rtrim( str_replace( $field_name, '', $input ), '_' );
			$address_1 = get_user_meta( get_current_user_id(), "{$form}_address_1", true );
			$house_number = get_user_meta( get_current_user_id(), "{$form}_house_number", true );
			if ( empty( $house_number ) && ! empty( $address_1 ) ) {
				$address_parts = $this->extract_street_number_suffix_from_address( $address_1 );
				if ( array_key_exists( $field_name, $address_parts ) ) {
					$default_value = $address_parts[$field_name];
				}
			}
		}
		return $default_value;
	}

	public function myparcel_suffix_limit( $fields, $country = '' ) {
		$form = str_replace( array('woocommerce_', '_fields'), '', current_filter() );
		if (isset($fields[$form.'_house_number_suffix'])) {
			$fields[$form.'_house_number_suffix']['maxlength'] = 4;
		}
		return $fields;
	}

	public function postcode_field_countries() {
		return apply_filters( 'wpo_wcnlpc_postcode_field_countries', array( 'NL' ) );
	}

	public function enable_fields_for_additional_countries( $countries ) {
		$show_for = get_option( 'woocommerce_wcnlpc_field_visibility', 'nl' );
		if ( $show_for  == 'nl_plus' ) {
			$additional_countries = get_option( 'woocommerce_wcnlpc_field_countries', array() );
			if ( is_array( $additional_countries ) ) {
				$countries = array_unique( array_merge( array_values( $additional_countries ), array_values( $countries ) ) );
			}
		} elseif ( $show_for == 'all') {
			$countries = array_keys( WC()->countries->countries );
		}

		return $countries;
	}

	public function nl_billing_fields( $fields, $country = '' ) {
		return $this->nl_checkout_fields( $fields, $country, 'billing');
	}

	public function nl_shipping_fields( $fields, $country = '' ) {
		return $this->nl_checkout_fields( $fields, $country, 'shipping');
	}

	/**
	 * Reorder checkout billing/shipping fields
	 *
	 * Some of this is also done by JS in the checkout,
	 * but this serves as a fallback/preparation mechanism
	 * 
	 * @param  array $fields Default fields.
	 * @return array $fields New fields.
	 */
	public function nl_checkout_fields( $fields, $country, $form ) {
		// disable on My Account page
		if ( is_account_page() && get_option( 'woocommerce_wcnlpc_enable_my_account' ) != 'yes' ) {
			return $fields;
		}

		if (isset($_fields['_country'])) {
			// some weird bug on the my account page
			$form = '';
		}

		if ( in_array( $country, $this->postcode_field_countries() ) ) {

			$keys = $country == 'NL' ? array( $form.'_postcode', $form.'_house_number', $form.'_house_number_suffix' ) : array( $form.'_house_number', $form.'_house_number_suffix', $form.'_postcode' );

			// put postcode, number, suffix before _address_1
			$fields = $this->array_move_before_key(
				$fields,
				$keys,
				$form.'_address_1'
			);

			$before_key = $country == 'NL' ? $form.'_city' : $form.'_house_number';

			// put street before city
			$fields = $this->array_move_before_key(
				$fields,
				array( $form.'_street_name' ),
				$before_key
			);

			if ( isset($fields[$form.'_address_1']) && isset($fields[$form.'_address_1']['priority']) ) {
				$address_1_priority = intval( $fields[$form.'_address_1']['priority'] );
				$city_priority = intval( $fields[$form.'_city']['priority'] );
				// set priorities
				if  ( $country == 'NL' ) {
					$fields[$form.'_postcode']['priority'] = $address_1_priority - 3;
					$fields[$form.'_house_number']['priority'] = $address_1_priority - 2;
					$fields[$form.'_house_number_suffix']['priority'] = $address_1_priority - 1;
					$fields[$form.'_street_name']['priority'] = $city_priority - 1;
				} else {
					$fields[$form.'_street_name']['priority'] = $address_1_priority - 4;
					$fields[$form.'_house_number']['priority'] = $address_1_priority - 3;
					$fields[$form.'_house_number_suffix']['priority'] = $address_1_priority - 2;
					$fields[$form.'_postcode']['priority'] = $address_1_priority - 1;
				}
			}

		}

		// set field classes / layout
		$fields[$form.'_street_name']['class']         = $this->get_checkout_field_classes( $form, 'street_name' );
		$fields[$form.'_postcode']['class']            = $this->get_checkout_field_classes( $form, 'postcode' );
		$fields[$form.'_house_number']['class']        = $this->get_checkout_field_classes( $form, 'house_number' );
		$fields[$form.'_house_number_suffix']['class'] = $this->get_checkout_field_classes( $form, 'house_number_suffix' );

		if ( get_option( 'woocommerce_wcnlpc_full_field_names', 'no' ) == 'yes' ) {
			$fields[$form.'_house_number']['label']        = __( 'House number', 'wpo_wcnlpc' );
			$fields[$form.'_house_number_suffix']['label'] = _x( 'Suffix', 'full string', 'wpo_wcnlpc' );
		}
		
		$fields[$form.'_house_number_suffix']['autocomplete'] = 'house_number_suffix';
		return $fields;
	}

	public function get_checkout_field_classes( $form, $field, $return = 'array' ) {
		$field_classes = array(
			'default'           => array(
				'street_name'         => array( 'form-row-wide' ),
				'postcode'            => array( 'form-row-first' ),
				'house_number'        => array( 'form-row-quart-first' ),
				'house_number_suffix' => array( 'form-row-quart' ),
			),
			'postcode_separate' => array(
				'postcode'            => array( 'form-row-wide' ),
				'house_number'        => array( 'form-row-first' ),
				'house_number_suffix' => array( 'form-row-last' ),
			),
			'all_separate'      => array(
				'postcode'            => array( 'form-row-wide' ),
				'house_number'        => array( 'form-row-wide' ),
				'house_number_suffix' => array( 'form-row-wide' ),
			),
			'one_line'          => array(
				'postcode'            => array( 'form-row-first' ),
				'house_number'        => array( 'form-row-quart-first' ),
				'house_number_suffix' => array( 'form-row-quart' ),
			),
		);

		$checkout_layout = get_option( 'woocommerce_wcnlpc_checkout_layout', 'default' );
		if ( !empty( $field_classes[$checkout_layout][$field] ) ) {
			$classes = $field_classes[$checkout_layout][$field];
		} elseif ( !empty( $field_classes['default'][$field] ) ) {
			$classes = $field_classes['default'][$field];
		} else {
			$classes = array();
		}

		$classes = apply_filters( 'wpo_wcnlpc_checkout_field_classes', $classes, $form, $field );

		if ( $return == 'string' ) {
			return esc_attr( implode( ' ', $classes ) );
		} else {
			return $classes;
		}
	}

	public function unset_postcode_before_city_nl ( $locale ) {
		foreach ( $this->postcode_field_countries() as $country_code ) {
			if ( isset( $locale[$country_code]['postcode_before_city'] ) ) {
				unset( $locale[$country_code]['postcode_before_city'] );
			}
		}


		return $locale;
	}

	public function clean_billing_street_duplicates ( $street ) {
		return $this->clean_street_duplicates( $street, 'billing' );
	}

	public function clean_shipping_street_duplicates ( $street ) {
		return $this->clean_street_duplicates( $street, 'shipping' );
	}

	public function clean_street_duplicates ( $street, $form ) {
		if (!empty($_POST['postcode_api_data'][$form])) {
			$postcode_data = json_decode(stripslashes_deep($_POST['postcode_api_data'][$form]), true);
			if (!empty($postcode_data['street'])) {
				$street = str_ireplace($postcode_data['street'].$postcode_data['street'], $postcode_data['street'], $street);
			}
		}
		return $street;
	}

	public function checkout_api_response_input () {
		?>
		<input type="hidden" name="postcode_api_data[billing]" id="billing_postcode_api_data" value="">
		<input type="hidden" name="postcode_api_data[shipping]" id="shipping_postcode_api_data" value="">
		<?php
	}

	public function save_postcode_data ( $order_id, $posted ) {
		if (!empty($_POST['postcode_api_data'])) {
			/*
			{"street":"Julianastraat",
			"houseNumber":30,
			"houseNumberAddition":"",
			"postcode":"2012ES",
			"city":"Haarlem",
			"municipality":"Haarlem",
			"province":"Noord-Holland",
			"rdX":103242,
			"rdY":487716,
			"latitude":52.37487801,
			"longitude":4.62714526,
			"bagNumberDesignationId":"0392200000029398",
			"bagAddressableObjectId":"0392010000029398",
			"addressType":"building",
			"purposes":["office"],
			"surfaceArea":643,
			"houseNumberAdditions":[""]}
			*/
			$forms = array( 'billing', 'shipping');
			$postcode_api_data = array();
			foreach ($forms as $form) {
				if (empty($_POST['postcode_api_data'][$form])) {
					continue;
				}
				$posted_data = json_decode(stripslashes_deep($_POST['postcode_api_data'][$form]), true);
				$postcode_api_data[$form] = array (
					"street"					=> $this->get_filtered_array_value( $posted_data, "street" ),
					"houseNumber"				=> $this->get_filtered_array_value( $posted_data, "houseNumber" ),
					"houseNumberAddition"		=> $this->get_filtered_array_value( $posted_data, "houseNumberAddition" ),
					"postcode"					=> $this->get_filtered_array_value( $posted_data, "postcode" ),
					"city"						=> $this->get_filtered_array_value( $posted_data, "city" ),
					"municipality"				=> $this->get_filtered_array_value( $posted_data, "municipality" ),
					"province"					=> $this->get_filtered_array_value( $posted_data, "province" ),
					"rdX"						=> $this->get_filtered_array_value( $posted_data, "rdX" ),
					"rdY"						=> $this->get_filtered_array_value( $posted_data, "rdY" ),
					"latitude"					=> $this->get_filtered_array_value( $posted_data, "latitude" ),
					"longitude"					=> $this->get_filtered_array_value( $posted_data, "longitude" ),
					"bagNumberDesignationId"	=> $this->get_filtered_array_value( $posted_data, "bagNumberDesignationId" ),
					"bagAddressableObjectId"	=> $this->get_filtered_array_value( $posted_data, "bagAddressableObjectId" ),
					"addressType"				=> $this->get_filtered_array_value( $posted_data, "addressType" ),
					"purposes"					=> $this->get_filtered_array_value( $posted_data, "purposes" ),
					"surfaceArea"				=> $this->get_filtered_array_value( $posted_data, "surfaceArea" ),
					"houseNumberAdditions"		=> $this->get_filtered_array_value( $posted_data, "houseNumberAdditions" ),
				);
			}

			if ( ! empty( $order = wc_get_order( $order_id ) ) ) {
				$order->update_meta_data( '_postcode_api_data', $postcode_api_data );
				$order->save_meta_data();
			}
		}
	}

	/**
	 * Helper function to move array elements (one or more) to a position before a specific key
	 * @param  array  $array      Main array to modify
	 * @param  mixed  $keys       Single key or array of keys of element(s) to move
	 * @param  string $before_key key to put elements in front of
	 * @return array              reordered array
	 */
	public function array_move_before_key ( $array, $keys, $before_key ) {
		// cast $key as array
		$keys = (array) $keys;

		if (!isset($array[$before_key])) {
			return $array;
		}

		$move = array();
		foreach ($keys as $key) {
			if (!isset($array[$key])) {
				continue;
			}
			$move[$key] = $array[$key];
			unset ($array[$key]);
		}

		$before_key_pos = array_search($before_key, array_keys($array));
		$new_array =
			array_slice($array, 0, $before_key_pos, true)
			+ $move
			+ array_slice($array, $before_key_pos, NULL, true);

		return $new_array;
	}

	public function get_filtered_array_value($array, $key) {
		if (isset($array[$key])) {
			$value = $array[$key];
			if (is_string($value)) {
				$value = esc_html( $value );
			} elseif (is_array($value)) {
				$value = array_filter( $value, 'esc_html' );
			}
			return $value;
		} else {
			return '';
		}
	}

	public function get_checkout_fields_hook_priority() {
		$priority = 100;
		// set later priority for woocommerce_billing_fields / woocommerce_shipping_fields when Checkout Field Editor is active
		if ( function_exists('thwcfd_is_locale_field') || function_exists('wc_checkout_fields_modify_billing_fields') || defined('THWCFD_VERSION') || defined('THWCFE_VERSION') ) {
			// ThemeHigh sets 1000 by default (version 3.1.3):
			// $hp_cf = apply_filters('thwcfd_woocommerce_checkout_fields_hook_priority', 1000);
			// we already set our own to 1001 in class-wcnlps-nlpostcode-fields.php so we should be later than that
			// (default being 9 & 100)
			$priority = 1010;
		}

		return apply_filters( 'wpo_wcnlpc_checkout_fields_hook_priority', $priority );
	}

	/**
	 * WooCommerce PayPal Checkout Gateway takes address data from PayPal itself and injects it into $_POST
	 * which WooCommerce then uses for the order data. The PayPal address data does not contain the split
	 * address fields and this will let the Postcode Checker throw an error stating that the number and
	 * street fields are missing. This function provides a workaround by splitting the single address line
	 * and setting the separate fields.
	 * 
	 * Similar behaviour happens using WooCommerce Gateway Stripe with Apple Pay, which we address here too.
	 */
	public function supplement_missing_checkout_data() {
		$is_paypal_checkout  = isset( $_POST['payment_method'] ) && ( 'ppec_paypal' === $_POST['payment_method'] );
		$is_stripe_apple_pay = isset( $_POST['payment_request_type'] ) && ( 'apple_pay' === wc_clean( wp_unslash( $_POST['payment_request_type'] ) ) );
		
		// Make sure the selected payment method is PayPal Checkout or Stripe Apple Pay
		if ( ! $is_paypal_checkout && ! $is_stripe_apple_pay ) {
			return;
		}

		foreach ( array( 'billing','shipping' ) as $address_type ) {
			if ( ! empty( $_POST["{$address_type}_country"] ) && ! empty( $_POST["{$address_type}_address_1"] ) ) {
				// only for the countries we have split fields for
				if ( in_array( $_POST["{$address_type}_country"], $this->postcode_field_countries() ) ) {
					$address_parts = $this->extract_street_number_suffix_from_address( $_POST["{$address_type}_address_1"] );
					if ( ! empty( $address_parts ) && is_array( $address_parts ) ) {
						foreach ($address_parts as $field => $value) {
							// only set if not already set
							if ( empty( $_POST["{$address_type}_{$field}"] ) && ! empty( $value ) ) {
								$_POST["{$address_type}_{$field}"] = $value;
							}
						}
					}
				}
			}
		}
	}

	public function extract_street_number_suffix_from_address( $address ) {
		$split_fields_regex = '~(?P<street_name>.*?)\s?(?P<house_number>\d{1,4})[/\s\-]{0,2}(?P<house_number_suffix>[a-zA-Z]{1}\d{1,3}|-\d{1,4}|\d{2}\w{1,2}|[a-zA-Z]{1}[a-zA-Z\s]{0,3})?$~';
		preg_match( $split_fields_regex, $address, $address_parts );
		$field_names = array( 'street_name', 'house_number', 'house_number_suffix' );
		$address_parts = array_intersect_key( $address_parts, array_flip( $field_names ) );
		return $address_parts;
	}
}

endif; // class_exists

return new WPO_WCNLPC_Checkout();
