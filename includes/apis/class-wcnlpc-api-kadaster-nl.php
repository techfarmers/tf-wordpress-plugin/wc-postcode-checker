<?php
namespace WPO\WC\Postcode_Checker\API;

use WPO\WC\Postcode_Checker\API\Exceptions\Address_Not_Found;
use WPO\WC\Postcode_Checker\API\Exceptions\Postcode_Invalid;
use WPO\WC\Postcode_Checker\API\Exceptions\Number_Invalid;
use WPO\WC\Postcode_Checker\API\Exceptions\Connection_Error;

// use GuzzleHttp\Client;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

if ( ! class_exists( '\\WPO\\WC\\Postcode_Checker\\API\\Kadaster_NL' ) ) :

class Kadaster_NL extends Generic {

	public function get_address() {
		$api_key = get_option( 'woocommerce_wcnlpc_api_key_kadaster' );
		$api_url = 'https://api.bag.kadaster.nl/lvbag/individuelebevragingen/v2/';

		$url = $api_url."adressen?postcode={$this->postcode}&huisnummer={$this->housenumber}";

		$headers = array (
			"Accept"        => "application/json, text/plain, */*",
			"Cache-Control" => "no-cache",
			"Content-Type"  => "application/json;charset=UTF-8",
			"X-Api-Key"     => $api_key,
		);

		$response = $this->get_json( $url, $headers );

		// Error:
		// '{"ExceptionResponse":{"message":"Er ging iets mis op de server. Wacht a.u.b. een moment en probeer het opnieuw."}}'
		if ( isset( $response['ExceptionResponse'] ) ) {
			throw new Connection_Error( $response['ExceptionResponse']['message'] );
		}

		if ( isset( $response['_embedded'] ) && isset( $response['_embedded']['adressen'] ) && ! empty( $response['_embedded']['adressen'] ) ) {
			
			// We only need the first address
			$address = reset( $response['_embedded']['adressen'] );

			$street = $address['openbareRuimteNaam'];
			$city = $address['woonplaatsNaam'];
		
			if ( ! empty( $street ) && ! empty( $city ) ) {
				$address = array(
					'postcode'           => $this->postcode,
					'housenumber'        => $this->housenumber,
					'housenumber_suffix' => $this->housenumber_addition,
					'street'             => $street,
					'city'               => $city,
					'data'               => $response,
				);
			} else {
				throw new Address_Not_Found( __('Incomplete address data', 'wpo_wcnlpc') );
			}
			return $address;
		} else {
			throw new Address_Not_Found( __('Address object not found', 'wpo_wcnlpc') );
		}
	}
}

endif; // class_exists
