<?php
namespace WPO\WC\Postcode_Checker\API;

use WPO\WC\Postcode_Checker\API\Exceptions\Address_Not_Found;
use WPO\WC\Postcode_Checker\API\Exceptions\Postcode_Invalid;
use WPO\WC\Postcode_Checker\API\Exceptions\Number_Invalid;
use WPO\WC\Postcode_Checker\API\Exceptions\Connection_Error;

// use GuzzleHttp\Client;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}


if ( !class_exists( '\\WPO\\WC\\Postcode_Checker\\API\\Postcodeapi_NU' ) ) :

class Postcodeapi_NU extends Generic {

	public function get_address() {
		$key = get_option( 'woocommerce_wcnlpc_api_key_postcodeapi_nu' );
		if ( apply_filters( 'wpo_wcnlpc_postcodeapi_sandbox', false ) ) {
			$url = "https://sandbox.postcodeapi.nu/v3/lookup/{$this->postcode}/{$this->housenumber}";
		} else {
			$url = "https://api.postcodeapi.nu/v3/lookup/{$this->postcode}/{$this->housenumber}";
		}

		$headers = array (
			"X-Api-Key" => $key,
		);
		$response = $this->get_json( $url, $headers );
		// printf("<pre>%s</pre>", var_export($response,true));die();

		if ( !empty($response['street']) && !empty($response['city']) ) {
			$address = array(
				'postcode'				=> $this->postcode,
				'housenumber'			=> $this->housenumber,
				'housenumber_suffix'	=> $this->housenumber_addition,
				'street'				=> $response['street'],
				'city'					=> $response['city'],
				'data'					=> $response,
			);
			return $address;
		} elseif (!empty($response['message'])) {
			throw new Connection_Error( $response['message'] );
		} elseif (!empty($response['error'])) {
			throw new Connection_Error( $response['error'] );
		} elseif (!empty($response['title'])) {
			throw new Connection_Error( $response['title'] );
		} else {
			throw new Address_Not_Found( __('Address not found', 'wpo_wcnlpc') );
		}
	}
}

endif; // class_exists
