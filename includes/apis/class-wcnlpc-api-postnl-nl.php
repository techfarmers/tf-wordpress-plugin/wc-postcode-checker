<?php
namespace WPO\WC\Postcode_Checker\API;

use WPO\WC\Postcode_Checker\API\Exceptions\Address_Not_Found;
use WPO\WC\Postcode_Checker\API\Exceptions\Postcode_Invalid;
use WPO\WC\Postcode_Checker\API\Exceptions\Number_Invalid;
use WPO\WC\Postcode_Checker\API\Exceptions\Connection_Error;

// use GuzzleHttp\Client;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}


if ( ! class_exists( '\\WPO\\WC\\Postcode_Checker\\API\\Postnl_NL' ) ) :

class Postnl_NL extends Generic {

	public function get_address() {
		$api_endpoint = $this->get_api_endpoint();
		
		if ( ! $api_endpoint ) {
			throw new Connection_Error( __( 'PostNL API endpoint not found!', 'wpo_wcnlpc' ) );
		}
		
		$args = array();
		switch ( $api_endpoint['method'] ) {
			case 'post': // legacy
				$args = array(
					'url'     => esc_url_raw( $api_endpoint['url'] ),
					'data'    => $api_endpoint['input_par'],
					'headers' => $api_endpoint['headers'],
				);
				break;
			case 'get':
				$args = array(
					'url'     => esc_url_raw( add_query_arg( $api_endpoint['input_par'], $api_endpoint['url'] ) ),
					'headers' => $api_endpoint['headers'],
					'auth'    => false,
				);
				break;
		}
		
		$response = apply_filters( 'wpo_wcnlpc_postnl_api_request_response', call_user_func_array( array( $this, $api_endpoint['method'] . '_json' ), $args ), $args, $this );
		$response = ! empty( $response ) && isset( $response[0] ) && is_array( $response[0] ) && ( array_key_exists( $api_endpoint['output_par']['street'], $response[0] ) ) ? $response[0] : $response;

		if ( ! empty( $response[$api_endpoint['output_par']['street']] ) && ! empty( $response[$api_endpoint['output_par']['city']] ) ) {
			$address = array(
				'postcode'           => $this->postcode,
				'housenumber'        => $this->housenumber,
				'housenumber_suffix' => $this->housenumber_addition,
				'street'             => $response[$api_endpoint['output_par']['street']],
				'city'               => $response[$api_endpoint['output_par']['city']],
				'data'               => $response,
			);
			return $address;
		} elseif ( ! empty( $response['errors'] ) && is_array( $response['errors'] ) ) {
			$error = array_pop( $response['errors'] );
			throw new Connection_Error( $error['detail'] );
		} elseif ( ! empty( $response['message'] ) ) {
			throw new Connection_Error( $response['message'] );
		} else {
			throw new Address_Not_Found( __( 'Address not found', 'wpo_wcnlpc' ) );
		}
	}
	
	public function get_api_endpoint() {
		$api_key     = get_option( 'woocommerce_wcnlpc_api_key_postnl_nl', '' );
		$current_api = get_option( 'woocommerce_wcnlpc_api_version_postnl_nl', 'v4::netherlands' );
		$api         = explode( '::', $current_api );
		$base_url    = 'https://api.postnl.nl';
		
		$endpoints   = apply_filters( 'wpo_wcnlpc_postnl_api_endpoints', array(
			'v1' => array( // legacy version
				'national' => array(
					'method'     => 'post',
					'url'        => "{$base_url}/address/national/v1/validate",
					'headers'    => array(
						'Content-Type' => 'Application/json',
						'apikey'       => $api_key,
					),
					'input_par'  => array(
						'PostalCode'  => $this->postcode,
						'HouseNumber' => absint( $this->housenumber ),
					),
					'output_par' => array(
						'street' => 'Street',
						'city'   => 'City',
					),
				),
			),
			'v4' => array(
				'netherlands' => array(
					'method'     => 'get',
					'url'        => "{$base_url}/v4/address/netherlands",
					'headers'    => array(
						'apikey' => $api_key,
					),
					'input_par'  => array(
						'postalCode'  => $this->postcode,
						'houseNumber' => absint( $this->housenumber ),
					),
					'output_par' => array(
						'street' => 'streetName',
						'city'   => 'cityName',
					),
				),
			),
		), $this );
		
		if ( isset( $api[0] ) && isset( $api[1] ) && isset( $endpoints[$api[0]][$api[1]] ) ) {
			return $endpoints[$api[0]][$api[1]];
		} else {
			return false;
		}
	}
	
}

endif; // class_exists
