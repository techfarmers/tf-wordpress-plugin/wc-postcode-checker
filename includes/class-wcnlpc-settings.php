<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

if ( ! class_exists( 'WPO_WCNLPC_Settings' ) ) :

class WPO_WCNLPC_Settings {
	
	function __construct()	{
		add_filter( 'plugin_action_links_'.WPO_WCNLPC_BASENAME, array( $this, 'settings_link' ) );

		add_action( 'woocommerce_settings_tabs_array', array( $this, 'add_settings_tab' ), 21 );
		add_action( 'woocommerce_settings_tabs_postcode_checker', array( $this, 'settings_tab' ) );
		add_action( 'woocommerce_update_options_postcode_checker', array( $this, 'update_settings' ) );
		add_action( 'woocommerce_admin_field_wpo_wcnlpc_test_connection', array( $this, 'test_connection_field' ) );
	}

	/**
	 * Add a new settings tab to the WooCommerce settings tabs array.
	 *
	 * @param array $settings_tabs Array of WooCommerce setting tabs & their labels, excluding ours.
	 * @return array $settings_tabs Array of WooCommerce setting tabs & their labels, including ours.
	 */
	public function add_settings_tab( $settings_tabs ) {
		$settings_tabs['postcode_checker'] = __( 'Postcode Checker', 'wpo_wcnlpc' );
		return $settings_tabs;
	}
	/**
	 * Uses the WooCommerce admin fields API to output settings via the @see woocommerce_admin_fields() function.
	 *
	 * @uses woocommerce_admin_fields()
	 * @uses $this->get_settings()
	 */
	public function settings_tab() {
		woocommerce_admin_fields( $this->get_settings() );
	}
	/**
	 * Uses the WooCommerce options API to save settings via the @see woocommerce_update_options() function.
	 *
	 * @uses woocommerce_update_options()
	 * @uses $this->get_settings()
	 */
	public function update_settings() {
		woocommerce_update_options( $this->get_settings() );

 		if ( version_compare( PHP_VERSION, '7.1', '>=' ) && extension_loaded( 'curl' ) && $this->autocomplete_enabled() ) {
			$_POST[ 'postcodenl_address_autocomplete_apiKey']    = get_option('woocommerce_wcnlpc_api_key');
			$_POST[ 'postcodenl_address_autocomplete_apiSecret'] = get_option('woocommerce_wcnlpc_api_secret');
			$options                                             = WPO_WCNLPC()->autocomplete->getOptions();
			
			$options->handleSubmit();
 		}
	}

	/**
	 * Add our API settings to the woocommerce Checkout settings tab
	 */
	public function api_settings ( $settings ) {
		$api_settings = $this->get_settings();

		// find end of checkout page options
		$checkout_page_options_end_key = array_search( array( 'type' => 'sectionend', 'id' => 'checkout_page_options' ), $settings );
		
		// $checkout_page_options_end_pos = $checkout_page_options_end_key+1;
		$checkout_page_options_end_pos = array_search( $checkout_page_options_end_key, array_keys( $settings ) ) + 1;
		
		// insert our api settings
		$new_settings = array_merge( array_slice( $settings, 0, $checkout_page_options_end_pos, true ), $api_settings, array_slice( $settings, $checkout_page_options_end_pos, null, true ) );


		return $new_settings;
	}

	public function get_settings() {
		$settings = array(

			array(
				'title'		=> __( 'Postcode Checker', 'wpo_wcnlpc' ),
				'type'		=> 'title',
				'id'		=> 'wcnlpc_api_options',
			),

			array(
				'title'    => __( 'Enable', 'wpo_wcnlpc' ),
				'id'       => 'woocommerce_wcnlpc_enable',
				'type'     => 'checkbox',
				'default'  => 'yes',
				'desc_tip' => true,
			),

			array(
				'title'    => __( 'Choose your API source', 'wpo_wcnlpc' ),
				'id'       => 'woocommerce_wcnlpc_checkout_api_source',
				'default'  => 'kadaster',
				'type'     => 'select',
				'options'  => array(
					'postcode_tech'  => __( 'postcode.tech (free)', 'wpo_wcnlpc' ),
					'kadaster'       => __( 'Het Kadaster / BAG (free)', 'wpo_wcnlpc' ),
					'postcodeapi_nu' => 'postcodeapi.nu',
					'postnl_nl'      => 'PostNL Adrescheck Nederland',
					'postcode_eu'    => 'postcode.eu',
				),
			),

			array(
				'title'    => __( 'API Token', 'wpo_wcnlpc' ),
				/* translators: postcode.tech URL */
				'desc'     => sprintf( __( 'Get your API token at %s and enter it here', 'wpo_wcnlpc' ), '<a href="https://postcode.tech/register">postcode.tech</a>' ),
				'id'       => $this->get_api_settings_fields( 'postcode_tech', 'token' ),
				'type'     => 'text',
				'css'      => 'min-width:450px;',
				'default'  => '',
				'custom_attributes' => array( 'data-api_source_specific' => 'postcode_tech' ),
			),

			array(
				'title'    => __( 'API key', 'wpo_wcnlpc' ),
				/* translators: kadaster.nl URL */
				'desc'     => sprintf( __( 'Get your API key at %s and enter it here', 'wpo_wcnlpc' ), '<a href="https://www.kadaster.nl/zakelijk/producten/adressen-en-gebouwen/bag-api-individuele-bevragingen">kadaster.nl</a>' ),
				'id'       => $this->get_api_settings_fields( 'kadaster', 'key' ),
				'type'     => 'text',
				'css'      => 'min-width:450px;',
				'default'  => '',
				'custom_attributes' => array( 'data-api_source_specific' => 'kadaster' ),
			),

			array(
				'title'    => __( 'API key', 'wpo_wcnlpc' ),
				'desc'     => __( 'Your PostNL API key', 'wpo_wcnlpc' ),
				'id'       => $this->get_api_settings_fields( 'postnl_nl', 'key' ),
				'type'     => 'text',
				'css'      => 'min-width:450px;',
				'default'  => '',
				'custom_attributes' => array( 'data-api_source_specific' => 'postnl_nl' ),
			),

			array(
				'title'    => __( 'Key', 'wpo_wcnlpc' ),
				'desc'     => sprintf( __( 'Get your API key at %s and enter it here', 'wpo_wcnlpc' ), '<a href="https://account.postcode.eu/register">postcode.eu</a>' ),
				'id'       => $this->get_api_settings_fields( 'postcode_eu', 'key' ),
				'type'     => 'text',
				'css'      => 'min-width:450px;',
				'default'  => '',
				'custom_attributes' => array( 'data-api_source_specific' => 'postcode_eu' ),
			),

			array(
				'title'    => __( 'Secret', 'wpo_wcnlpc' ),
				'desc'     => __( 'Your API Secret from postcode.eu', 'wpo_wcnlpc' ),
				'id'       => $this->get_api_settings_fields( 'postcode_eu', 'secret' ),
				'type'     => 'text',
				'css'      => 'min-width:450px;',
				'default'  => '',
				'custom_attributes' => array( 'data-api_source_specific' => 'postcode_eu' ),
			),

			array(
				'title'    => __( 'API Key', 'wpo_wcnlpc' ),
				'desc'     => sprintf( __( 'Get your API key at %s and enter it here', 'wpo_wcnlpc' ), '<a href="https://www.postcodeapi.nu/">postcodeapi.nu</a>' ),
				'id'       => $this->get_api_settings_fields( 'postcodeapi_nu', 'key' ),
				'type'     => 'text',
				'css'      => 'min-width:450px;',
				'default'  => '',
				'custom_attributes' => array( 'data-api_source_specific' => 'postcodeapi_nu' ),
			),
			
			array(
				'title'    => __( 'Test connection', 'wpo_wcnlpc' ),
				'id'       => 'woocommerce_wcnlpc_api_test_connection',
				'type'     => 'wpo_wcnlpc_test_connection',
				'css'      => 'min-width:450px;',
				'default'  => '',
			),

			array(
				'title'    => __( 'Enable international auto-complete', 'wpo_wcnlpc' ),
				'id'       => 'woocommerce_wcnlpc_enable_autocomplete',
				'type'     => 'checkbox',
				'default'  => 'no',
				'desc'     => version_compare( PHP_VERSION, '7.1', '<' ) ? __( 'requires PHP 7.1 or higher', 'wpo_wcnlpc' ) : __('Address auto-completion for Belgium, Luxembourg, France, Germany, Austria, Switzerland and the United Kingdom', 'wpo_wcnlpc' ),
				'custom_attributes' => version_compare( PHP_VERSION, '7.1', '<' ) ?
					array( // autocomplete requires PHP 7.2+ 
						'disabled' => 'disabled',
						'data-api_source_specific' => 'postcode_eu'
					) : array(
						'data-api_source_specific' => 'postcode_eu'
					),
			),

			array(
				'title'    => __( 'Show postcode.eu logo', 'wpo_wcnlpc' ),
				'desc'     => __( 'Show background image in the postcode.eu international auto-complete search field', 'wpo_wcnlpc' ),
				'id'       => 'woocommerce_wcnlpc_postcode_eu_logo',
				'type'     => 'checkbox',
				'default'  => 'no',
				'custom_attributes' => array( 'data-api_source_specific' => 'postcode_eu' ),
			),

			array(
				'title'    => __( 'Layout for postcode & number fields', 'wpo_wcnlpc' ),
				'id'       => 'woocommerce_wcnlpc_checkout_layout',
				'default'  => 'one_line',
				'type'     => 'radio',
				'desc_tip' =>  __( 'Set your preference for the layout of the postcode checkout fields (postcode, house number, house number suffix)', 'wpo_wcnlpc' ),
				'options'  => array(
					'one_line'           => __( 'All on one line', 'wpo_wcnlpc' ),
					'postcode_separate'  => __( 'Postcode separate', 'wpo_wcnlpc' ),
					'all_separate'       => __( 'Each field on a new line', 'wpo_wcnlpc' ),
				),
			),

			array(
				'title'    => __( 'Show separate street & number fields for:', 'wpo_wcnlpc' ),
				'id'       => 'woocommerce_wcnlpc_field_visibility',
				'default'  => 'nl',
				'type'     => 'select',
				'class'    => 'wc-enhanced-select',
				'css'      => 'min-width: 350px;',
				'options'  => array(
					'nl'      => __( 'Only the Netherlands', 'wpo_wcnlpc' ),
					'all'     => __( 'All countries', 'wpo_wcnlpc' ),
					'nl_plus' => __( 'The Netherlands plus these additional countries', 'wpo_wcnlpc' ),
				),
			),

			array(
				'title'   => '',
				'desc'    => '',
				'id'      => 'woocommerce_wcnlpc_field_countries',
				'css'     => 'min-width: 350px;',
				'default' => '',
				'type'    => 'multi_select_countries',
			),

			array(
				'title'    => __( 'Street & city fields (NL)', 'wpo_wcnlpc' ),
				'id'       => 'woocommerce_wcnlpc_checkout_street_city_visibility',
				'default'  => 'show',
				'type'     => 'radio',
				'desc_tip' =>  __( 'Choose how/when you want to show the street & city fields', 'wpo_wcnlpc' ),
				'options'  => array(
					'show'     => __( 'Show (default: always editable)', 'wpo_wcnlpc' ),
					'readonly' => __( 'Read only (editable when postcode not found)', 'wpo_wcnlpc' ),
					'hide'     => __( 'Hidden (editable when postcode not found)', 'wpo_wcnlpc' ),
				),
			),

			array(
				'title'    => __( 'Full field names', 'wpo_wcnlpc' ),
				'desc'     => __( 'Show full names of the Nr. & Suffix fields in the checkout (normally abbreviated to save space)', 'wpo_wcnlpc' ),
				'id'       => 'woocommerce_wcnlpc_full_field_names',
				'type'     => 'checkbox',
				'default'  => 'no',
				'desc_tip' => true,
			),

			array(
				'title'    => __( 'Show spinner', 'wpo_wcnlpc' ),
				'desc'     => __( 'Show a spinner when address is being validated (only for settings "Show" and "Read only")', 'wpo_wcnlpc' ),
				'id'       => 'woocommerce_wcnlpc_show_spinner',
				'type'     => 'checkbox',
				'default'  => 'yes',
				'desc_tip' => true,
			),

			array(
				'title'    => __( 'Enable on My Account', 'wpo_wcnlpc' ),
				'id'       => 'woocommerce_wcnlpc_enable_my_account',
				'type'     => 'checkbox',
				'default'  => 'no',
			),

			array(
				'title'    => __( 'Profile data fallback', 'wpo_wcnlpc' ),
				'desc'     => __( 'Customers that placed an order on your site before the Postcode Checker was installed will not have their house number filled in by default (as a separate field), and when they go through the checkout for the first time after you have activated the Postcode Chcker, they will have to fill this once. This fallback extracts the separate fields from the default address line.', 'wpo_wcnlpc' ),
				'id'       => 'woocommerce_wcnlpc_enable_profile_data_fallback',
				'type'     => 'checkbox',
				'default'  => 'no',
				'desc_tip' => true,
			),

			array( 'type' => 'sectionend', 'id' => 'wcnlpc_api_options' ),
		);

		return apply_filters( 'wc_settings_tab_postcode_checker_settings', $settings );
	}

	public function settings_link( $links ) {
		$url           = admin_url( 'admin.php?page=wc-settings&tab=postcode_checker' );
		$settings_link = sprintf( '<a href="%s">%s</a>', $url, __( 'Settings', 'wpo_wcnlpc' ) );
		
		array_unshift( $links, $settings_link );
		
		return $links;
	}

	public function validation_enabled() {
		return WPO_WCNLPC()->validation_enabled();
	}

	public function autocomplete_enabled() {
		if ( 'yes' !== get_option( 'woocommerce_wcnlpc_enable_autocomplete', 'no' ) ) {
			return false;
		}
		
		$api_source = get_option( 'woocommerce_wcnlpc_checkout_api_source', 'kadaster' );
		
		if ( 'postcode_eu' === $api_source && $this->api_keys_set( $api_source ) ) {
			return true;
		} else {
			return false;
		}
	}
	
	public function test_connection_field( $value ) {		
		$description = $tooltip_html = '';
		
		if ( class_exists( 'WC_Admin_Settings' ) ) {
			$field_description = \WC_Admin_Settings::get_field_description( $value );
			$description       = $field_description['description'];
			$tooltip_html      = $field_description['tooltip_html'];
		}
		?>
			<tr valign="top"<?php echo $value['row_class'] ? ' class="' . esc_attr( $value['row_class'] ) . '"' : '' ?>">
				<th scope="row" class="titledesc">
					<label for="<?php echo esc_attr( $value['id'] ); ?>"><?php echo esc_html( $value['title'] ); ?> <?php echo $tooltip_html; // WPCS: XSS ok. ?></label>
				</th>
				<td class="forminp">
					<?php echo $description; // WPCS: XSS ok. ?>
					<a href="#" id="<?php echo esc_attr( $value['id'] ); ?>" class="button"><?php echo esc_html( $value['title'] ); ?></a>
					<div class="wpo-spinner" style="display:none; width:16px; height:30px; line-height:30px; margin-left:6px;"></div>
					<div class="notice inline" style="display:none; width:450px;"><p></p></div>
				</td>
			</tr>
		<?php
	}
	
	/**
	 * Get API settings fields
	 *
	 * @param  string        $api_source
	 * @param  string        $type        can be 'key', 'secret' or 'token'
	 * @return string|array
	 */
	public function get_api_settings_fields( string $api_source = 'all', string $field_type = 'all' ) {
		$prefix = 'woocommerce_wcnlpc_api_';
		$fields = apply_filters( 'wpo_wcnlpc_postcode_api_settings_fields', array(
			'postcode_eu' => array(
				'key'    => $prefix . 'key',
				'secret' => $prefix . 'secret',
				'token'  => '',
			),
			'postcodeapi_nu' => array(
				'key'    => $prefix . 'key_postcodeapi_nu',
				'secret' => '',
				'token'  => '',
			),
			'kadaster' => array(
				'key'    => $prefix . 'key_kadaster',
				'secret' => '',
				'token'  => '',
			),
			'postnl_nl' => array(
				'key'    => $prefix . 'key_postnl_nl',
				'secret' => '',
				'token'  => '',
			),
			'postcode_tech' => array(
				'key'    => '',
				'secret' => '',
				'token'  => $prefix . 'token_postcode_tech',
			),
		) );
		
		if ( 'all' !== $api_source && isset( $fields[ $api_source ] ) ) {
			if ( 'all' !== $field_type && in_array( $field_type, array( 'key', 'secret', 'token' ) ) ) {
				return $fields[ $api_source ][ $field_type ];
			} else {
				return $fields[ $api_source ];
			}
		}
		
		return $fields;
	}
	
	/**
	 * Check if the key, secret or token is set for an API source
	 *
	 * @param  string $api_source
	 * @return boolean
	 */
	public function api_keys_set( string $api_source ): bool {
		if ( empty( $api_source ) ) {
			$api_source = get_option( 'woocommerce_wcnlpc_checkout_api_source', 'kadaster' );
		}
		
		$api_fields = $this->get_api_settings_fields( $api_source );
		
		if ( ! empty( $api_fields ) ) {
			$key    = isset( $api_fields['key'] ) ? $api_fields['key'] : '';
			$secret = isset( $api_fields['secret'] ) ? $api_fields['secret'] : '';
			$token  = isset( $api_fields['token'] ) ? $api_fields['token'] : '';
			
			if ( ! empty( $key ) && ! empty( $secret ) ) {
				return ! empty( get_option( $key ) ) && ! empty( get_option( $secret ) );
			} elseif ( ! empty( $token ) ) {
				return ! empty( get_option( $token ) );
			} elseif ( ! empty( $key ) ) {
				return ! empty( get_option( $key ) );
			}
		}
		
		return true;
	}
	
	/**
	 * Maybe save values to the API settings fields
	 *
	 * @param  string $api_source
	 * @param  array  $api_fields
	 * @return void
	 */
	public function maybe_save_api_settings_fields( string $api_source, array $api_fields ): void {
		if ( empty( $api_source ) || empty( $api_fields ) ) {
			return;
		}
		
		foreach ( $api_fields as $field_type => $field_value ) {
			update_option( $this->get_api_settings_fields( esc_attr( $api_source ), esc_attr( $field_type ) ), esc_attr( $field_value ) );
		}
	}

}

endif; // class_exists

return new WPO_WCNLPC_Settings();