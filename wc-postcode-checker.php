<?php
/**
 * Plugin Name:          WooCommerce NL Postcode Checker
 * Plugin URI:           https://wpovernight.com/downloads/woocommerce-postcode-checker/
 * Description:          Automatically validate dutch postcodes and fill in town and street name
 * Version:              2.10.10
 * Author:               WP Overnight
 * Author URI:           https://wpovernight.com
 * License:              GPLv2 or later
 * License URI:          https://opensource.org/licenses/gpl-license.php
 * Text Domain:          wpo_wcnlpc
 * Domain Path:          /languages
 * WC requires at least: 3.0
 * WC tested up to:      9.0
 */


if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

if ( ! class_exists( 'WPO_WC_Postcode_Checker' ) ) :

class WPO_WC_Postcode_Checker {

	public $version = '2.10.10';

	protected static $_instance = null;

	protected $autocomplete = null;

	protected $settings = null;

	public $checkout;

	/**
	 * @var WPO_Updater|WPO_Update_Helper
	 */
    private $updater;

	/**
	 * Main Plugin Instance
	 *
	 * Ensures only one instance of plugin is loaded or can be loaded.
	 */
	public static function instance() {
		if ( is_null( self::$_instance ) ) {
			self::$_instance = new self();
		}
		return self::$_instance;
	}

	/**
	 * Constructor
	 */
	public function __construct() {
		$this->define( 'WPO_WCNLPC_VERSION', $this->version );
		$this->define( 'WPO_WCNLPC_BASENAME', plugin_basename( __FILE__ ) );

		// load the localisation & classes
		add_action( 'plugins_loaded', array( $this, 'translations' ) );
		add_action( 'plugins_loaded', array( $this, 'plugins_loaded' ) );
		add_action( 'init', array( $this, 'load_classes' ) );

		// Load the updater
		add_action( 'init', array( $this, 'load_updater' ), 0 );

		// run lifecycle methods
		if ( is_admin() && ! defined( 'DOING_AJAX' ) ) {
			add_action( 'wp_loaded', array( $this, 'do_install' ) );
		}

		// Declare Woo features compatibility.
		add_action( 'before_woocommerce_init', array( $this, 'woo_features_compatibility' ) );
		
		// admin notices
		add_action( 'admin_notices', array( $this, 'api_missing_notice' ) );
	}

	/**
	 * Define constant if not already set
	 * @param  string $name
	 * @param  string|bool $value
	 */
	private function define( $name, $value ) {
		if ( ! defined( $name ) ) {
			define( $name, $value );
		}
	}

	/**
	 * Run the updater scripts from the WPO Sidekick
	 * @return void
	 */
	public function load_updater() {
		// Init updater data
		$item_name		= 'WooCommerce Postcode Checker';
		$file			= __FILE__;
		$license_slug	= 'wpo_wcnlpc_license';
		$version		= WPO_WCNLPC_VERSION;
		$author			= 'Ewout Fernhout';

		// load updater
		if ( class_exists( 'WPO_Updater' ) ) { // WP Overnight Sidekick plugin
			$this->updater = new WPO_Updater( $item_name, $file, $license_slug, $version, $author );
		} else { // bundled updater
			$updater_helper_file = $this->plugin_path() . '/updater/update-helper.php';

			if ( ! class_exists( 'WPO_Update_Helper' ) && file_exists( $updater_helper_file ) ) {
				include_once $updater_helper_file;
			}

			if ( class_exists( 'WPO_Update_Helper' ) ) {
				$this->updater = new WPO_Update_Helper( $item_name, $file, $license_slug, $version, $author );
			}
		}

		// if no license is entered, show notice in plugin settings page
		if ( is_callable( array( $this->updater, 'license_is_active' ) ) && ! $this->updater->license_is_active() ) {
			add_action( 'woocommerce_settings_postcode_checker', array( $this, 'no_active_license_message' ), -10 );
		}
	}

	public function no_active_license_message() {
		if( class_exists('WPO_Updater') ) {
			$activation_url = esc_url_raw( network_admin_url( 'admin.php?page=wpo-license-page' ) );
		} else {
			$activation_url = esc_url_raw( network_admin_url( 'plugins.php?s=Postcode+Checker' ) );
		}
		?>
		<div class="notice notice-warning inline">
			<p>
				<?php /* translators: click here */ ?>
				<?php printf(__( "Your license has not been activated on this site, %s to enter your license key.", 'wpo_wcnlpc' ), '<a href="'.$activation_url.'">'.__( 'click here', 'wpo_wcnlpc' ).'</a>' ); ?>
			</p>
		</div>
		<?php
	}
	
	/**
	 * Displays the admin note if the API source is not set or if the API key/token for the source is missing.
	 */
	public function api_missing_notice() {
		$enabled            = get_option( 'woocommerce_wcnlpc_enable' );
		$api_source         = get_option( 'woocommerce_wcnlpc_checkout_api_source', '' );
		$api_key            = '';
		$notice_description = '';
		$url                = esc_url_raw( admin_url( 'admin.php?page=wc-settings&tab=postcode_checker' ) );

		switch ( $api_source ) {
			case 'postcode_tech':
				$api_key            = get_option( 'woocommerce_wcnlpc_api_token_postcode_tech', '' );
				/* translators: click here link */
				$notice_description = __( 'The postcode.tech (free) API token is missing in the Postcode Checker settings, %s to enter your API token.', 'wpo_wcnlpc' );
				break;
			case 'kadaster':
				$api_key            = get_option( 'woocommerce_wcnlpc_api_key_kadaster', '' );
				/* translators: click here link */
				$notice_description = __( 'The Kadaster/BAG API key is missing in the Postcode Checker settings, %s to enter your API key.', 'wpo_wcnlpc' );
				break;
			case 'postcodeapi_nu':
				$api_key            = get_option( 'woocommerce_wcnlpc_api_key_postcodeapi_nu', '' );
				/* translators: click here link */
				$notice_description = __( 'The postcodeapi.nu API key is missing in the Postcode Checker settings, %s to enter your API key.', 'wpo_wcnlpc' );
				break;
			case 'postnl_nl':
				$api_key            = get_option( 'woocommerce_wcnlpc_api_key_postnl_nl', '' );
				/* translators: click here link */
				$notice_description = __( 'The PostNL Adrescheck Nederland API key is missing in the Postcode Checker settings, %s to enter your API key.', 'wpo_wcnlpc' );
				break;
			case 'postcode_eu':
				$api_key            = get_option( 'woocommerce_wcnlpc_api_key', '' );
				/* translators: click here link */
				$notice_description = __( 'The postcode.eu key is missing in the Postcode Checker settings, %s to enter your key.', 'wpo_wcnlpc' );
				break;
			case '':
				/* translators: click here link */
				$notice_description = __( 'The API source is missing, %s to enter your API source.', 'wpo_wcnlpc' );
				break;
		}
		
		if ( 'yes' === $enabled && empty( $api_key ) ) {
			$checkout_notice = __( 'Please note that without an API key, our separate street and house number fields won\'t appear in the checkout.', 'wpo_wcnlpc' );

			?>
			<div class="notice notice-warning">
				<p>
					<?php
						printf(
							$notice_description . '<br />' . $checkout_notice,
							'<a href="' . $url . '">' . __( 'click here', 'wpo_wcnlpc' ) . '</a>'
						);
					?>
				</p>
			</div>
			<?php
		} 
	}

	/**
	 * Load the translation / textdomain files
	 * 
	 * Note: the first-loaded translation file overrides any following ones if the same translation is present
	 */
	public function translations() {
		$locale = apply_filters( 'plugin_locale', get_locale(), 'wpo_wcnlpc' );
		$dir    = trailingslashit( WP_LANG_DIR );

		/**
		 * Frontend/global Locale. Looks in:
		 *
		 * 		- WP_LANG_DIR/wc-postcode-checker/wpo_wcnlpc-LOCALE.mo
		 * 	 	- WP_LANG_DIR/plugins/wpo_wcnlpc-LOCALE.mo
		 * 	 	- wc-postcode-checker/languages/wpo_wcnlpc-LOCALE.mo (which if not found falls back to:)
		 * 	 	- WP_LANG_DIR/plugins/wpo_wcnlpc-LOCALE.mo
		 */
		load_textdomain( 'wpo_wcnlpc', $dir . 'wc-postcode-checker/wpo_wcnlpc-' . $locale . '.mo' );
		load_textdomain( 'wpo_wcnlpc', $dir . 'plugins/wpo_wcnlpc-' . $locale . '.mo' );
		load_plugin_textdomain( 'wpo_wcnlpc', false, dirname( plugin_basename(__FILE__) ) . '/languages' );
	}


	/**
	 * Perform any actions we may want to do as soon as possible
	 */
	public function plugins_loaded() {
		if ( version_compare( PHP_VERSION, '7.1', '>=' ) && extension_loaded('curl') && $this->is_postcodenl_activated() === false ) {
			$this->autoload_postcodenl_autocomplete();
			if ( $this->settings()->autocomplete_enabled() ) {
				$this->autocomplete();
			}
		}

		// If validation is not enabled, we disable the MyParcel & PostNL checkout setting to avoid unnecessary complexity
		$this->maybe_disable_myparcel_field_setting();
	}

	public function maybe_disable_myparcel_field_setting() {
		if ( $this->validation_enabled() === false ) {
			return;
		}

		$myparcel = $this->get_myparcel_version();
		$postnl   = $this->get_postnl_version();
		if ( ( $myparcel || $postnl ) && version_compare( PHP_VERSION, '5.3', '>=' ) ) {
			$plugin_version = $myparcel ? $myparcel : $postnl;
			// strip option from get_option()
			$checkout_settings_filter = $myparcel ? 'option_woocommerce_myparcel_checkout_settings' : 'option_woocommerce_postnl_checkout_settings';
			$wcpc_data = array (
				'version' => $plugin_version,
				'name'    => $myparcel ? 'myparcel' : 'postnl',
			);
			add_filter($checkout_settings_filter, function( $value ) use ( $wcpc_data ) {
				if (is_array($value)) {
					if ( version_compare( $wcpc_data['version'], '4.0.0', '>=' ) ) { // 4.0+
						$value['use_split_address_fields'] = 0;
					} else {
						unset($value['use_split_address_fields']);
					}
				}
				return $value;
			});
			// hide the option to avoid confusion
			add_action('admin_enqueue_scripts', function() use ( $wcpc_data ){
				if ( version_compare( $wcpc_data['version'], '4.0.0', '>=' ) ) { // 4.0+
					$selectors = array(
						"#use_split_address_fields",
						"input[name='woocommerce_{$wcpc_data['name']}_checkout_settings[use_split_address_fields]']"
					);
					$script_handle = $wcpc_data['name'] == 'myparcel' ? 'wcmp-admin' : 'wcpn-admin';
					wp_add_inline_script( $script_handle, 'jQuery(function($) { $("'.implode(', ', $selectors).'").closest("tr").hide(); });' );
				} else {
					wp_add_inline_style( 'wcmp-admin-styles', '.use_split_address_fields { display: none !important; }' );
				}
			}, 99999 );

			// fake the split address fields option for the frontend scripts
			if ( version_compare( $wcpc_data['version'], '4.0.0', '>=' ) ) { // 4.0
				add_action('wp_enqueue_scripts',function(){
					// wp_localize_script doesn't allow for specific value modification
					wp_add_inline_script( 'wc-myparcel-frontend', '
					if ( typeof MyParcelDisplaySettings !== "undefined" && typeof MyParcelDisplaySettings.isUsingSplitAddressFields !== "undefined" ) {
						MyParcelDisplaySettings.isUsingSplitAddressFields = 1;
					}' );
					wp_add_inline_script( 'wc-postnl-frontend', '
					if ( typeof PostNLDisplaySettings !== "undefined" && typeof PostNLDisplaySettings.isUsingSplitAddressFields !== "undefined" ) {
						PostNLDisplaySettings.isUsingSplitAddressFields = 1;
					}' );
				}, 101 ); // MyParcel adds theirs at 100
			}
		}
	}

	/**
	 * Auto-load in-accessible properties on demand.
	 *
	 * @param mixed $key Key name.
	 * @return mixed
	 */
	public function __get( $key ) {
		if ( in_array( $key, array( 'settings', 'autocomplete' ), true ) ) {
			return $this->$key();
		}
	}

	public function settings() {
		if (empty($this->settings)) {
			$this->settings = include_once( 'includes/class-wcnlpc-settings.php' );
		}
		return $this->settings;
	}

	public function autocomplete() {
		if (empty($this->autocomplete)) {
			$className = '\PostcodeNl\AddressAutocomplete\Main';
			$this->autocomplete = new $className();
		}
		return $this->autocomplete;
	}

	/**
	 * Load the main plugin classes and functions
	 */
	public function includes() {
		$this->settings();
		include_once( 'includes/class-wcnlpc-order-util.php' );
		include_once( 'includes/class-wcnlpc-api.php' );
		include_once( 'includes/class-wcnlpc-meta-boxes.php' );
		$this->checkout = include_once( 'includes/class-wcnlpc-checkout.php' );
		include_once( 'includes/class-wcnlpc-assets.php' );
	}

	public function autoload_postcodenl_autocomplete() {
		spl_autoload_register(static function(string $className) {
			if (strpos($className, 'PostcodeNl\\InternationalAutocomplete\\') === 0) {
				/** @noinspection PhpIncludeInspection */
				require_once plugin_dir_path(__FILE__) . 'libraries/' . str_replace('\\', '/', $className) . '.php';
				return;
			}
			if (strpos($className, 'PostcodeNl\\AddressAutocomplete\\') === 0) {
				/** @noinspection PhpIncludeInspection */
				$baseClassName = str_replace('PostcodeNl\\AddressAutocomplete\\', '', $className );
				require_once plugin_dir_path(__FILE__) . 'includes/autocomplete/' . str_replace('\\', '/', $baseClassName) . '.php';
				return;
			}
		});
	}

	/**
	 * Instantiate classes when woocommerce is activated
	 */
	public function load_classes() {
		if ( $this->is_woocommerce_activated() === false ) {
			add_action( 'admin_notices', array ( $this, 'need_woocommerce' ) );
			return;
		}

		if ( version_compare( PHP_VERSION, '5.3', '<' ) ) {
			add_action( 'admin_notices', array ( $this, 'required_php_version' ) );
			return;
		}

		// all systems ready - GO!
		$this->includes();
	}

	/**
	 * Check if woocommerce is activated
	 */
	public function is_woocommerce_activated() {
		$blog_plugins = get_option( 'active_plugins', array() );
		$site_plugins = get_site_option( 'active_sitewide_plugins', array() );

		if ( in_array( 'woocommerce/woocommerce.php', $blog_plugins ) || isset( $site_plugins['woocommerce/woocommerce.php'] ) ) {
			return true;
		} else {
			return false;
		}
	}

	public function is_postcodenl_activated() {
		if ( class_exists( '\\PostcodeNl\\AddressAutocomplete\\Main' ) ) {
			return true;
		}
		$blog_plugins = get_option( 'active_plugins', array() );
		foreach ($blog_plugins as $plugin) {
			if ( strpos($plugin, 'postcodenl-address-autocomplete.php') !== false ) {
				return true;
			}
		}
		$site_plugins = array_keys( get_site_option( 'active_sitewide_plugins', array() ) );
		foreach ($site_plugins as $plugin) {
			if ( strpos($plugin, 'postcodenl-address-autocomplete.php') !== false ) {
				return true;
			}
		}
		// prevent PostcodeNl from crashing during activation
		if ( isset($_GET['action']) && $_GET['action'] == 'activate' && isset($_GET['plugin']) && strpos($_GET['plugin'], 'postcodenl-address-autocomplete.php') !== false ) {
			return true;
		}
		return false;
	}

	public function get_myparcel_version() {
		$constants = array(
			'WC_MYPARCEL_NL_VERSION', // 4.0.0+
			'WC_MYPARCEL_BE_VERSION', // 4.0.0+
			'WC_MYPARCEL_VERSION', // 1.5.6 - 3.2.1
		);

		foreach ( $constants as $name ) {
			if ( defined( $name ) ) {
				return constant( $name );
			}
		}

		// early detection (before plugin class is instantiated)
		if ( function_exists( 'WCMYPA' ) ) { // 4.0.0+
			return '4.0.0';
		}
		if ( function_exists( 'WooCommerce_MyParcel' ) ) { // 2.0 - current
			return '2.0.0';
		}
		
		return false; // no MyParcel found
	}

	public function get_postnl_version() {
		$constants = array(
			'WC_POSTNL_VERSION' // 2.5.0+
		);

		foreach ( $constants as $name ) {
			if ( defined( $name ) ) {
				return constant( $name );
			}
		}
		
		return false; // no PostNL found
	}

	public function validation_enabled(): bool {
		$enabled = get_option( 'woocommerce_wcnlpc_enable' );
		$enabled = apply_filters( 'wpo_wcnlpc_validation_enabled', wc_string_to_bool( $enabled ) );

		if ( ! $enabled ) {
			return false;
		}

		// enabled = set, now check api keys
		$api_source = get_option( 'woocommerce_wcnlpc_checkout_api_source', 'kadaster' );

		return $this->settings()->api_keys_set( $api_source );
	}
	
	/**
	 * WooCommerce not active notice.
	 */
	 
	public function need_woocommerce() {
		/* translators: <a> tags */
		$error = sprintf( __( 'WooCommerce NL Postcode Checker requires %1$sWooCommerce%2$s to be installed & activated!' , 'wpo_wcnlpc' ), '<a href="https://wordpress.org/plugins/woocommerce/">', '</a>' );

		$message = '<div class="error"><p>' . $error . '</p></div>';
	
		echo $message;
	}

	/**
	 * PHP version requirement notice
	 */
	
	public function required_php_version() {
		$error = __( 'WooCommerce NL Postcode Checker requires PHP 5.3 or higher (5.6 or higher recommended).', 'wpo_wcnlpc' );
		$how_to_update = __( 'How to update your PHP version', 'wpo_wcnlpc' );
		$message = sprintf('<div class="error"><p>%s</p><p><a href="%s">%s</a></p></div>', $error, 'http://docs.wpovernight.com/general/how-to-update-your-php-version/', $how_to_update);
	
		echo $message;
	}


	/** Lifecycle methods *******************************************************
	 * Because register_activation_hook only runs when the plugin is manually
	 * activated by the user, we're checking the current version against the
	 * version stored in the database
	****************************************************************************/

	/**
	 * Handles version checking
	 */
	public function do_install() {
		$version_setting = 'wpo_wcnlpc_version';
		$installed_version = get_option( $version_setting );

		// installed version lower than plugin version?
		if ( version_compare( $installed_version, $this->version, '<' ) ) {

			if ( ! $installed_version ) {
				$this->install();
			} else {
				$this->upgrade( $installed_version );
			}

			// new version number
			update_option( $version_setting, $this->version );
		}
	}


	/**
	 * Plugin install method. Perform any installation tasks here
	 */
	protected function install() {
		// stub
	}

	/**
	 * Plugin upgrade method.  Perform any required upgrades here
	 *
	 * @param string $installed_version the currently installed ('old') version
	 */
	protected function upgrade( $installed_version ) {
		if ( version_compare( $installed_version, '2.0', '<' ) ) {
			update_option( 'woocommerce_wcnlpc_checkout_api_source', 'postcode_nl' );
			$key = get_option( 'woocommerce_wcnlpc_api_key' );
			$secret = get_option( 'woocommerce_wcnlpc_api_secret' );
			if ( !empty($key) && !empty($secret) ) {
				update_option( 'woocommerce_wcnlpc_enable', 'yes' );
			}
		}
		
		// postcode_nl => postcode_eu
		if ( version_compare( $installed_version, '2.10.8-beta-1', '<' ) ) {
			$api_source = get_option( 'woocommerce_wcnlpc_checkout_api_source', 'kadaster' );
			
			if ( 'postcode_nl' === $api_source ) {
				update_option( 'woocommerce_wcnlpc_checkout_api_source', 'postcode_eu' );
			}
			
			$show_logo = get_option( 'woocommerce_wcnlpc_postcode_nl_logo', 'no' );
			update_option( 'woocommerce_wcnlpc_postcode_eu_logo', $show_logo );
			
			delete_option( 'woocommerce_wcnlpc_postcode_nl_logo' );
			delete_option( 'woocommerce_wcnlpc_postcode_nl_server' );
		}
	}		

	/**
	 * Get the plugin url.
	 * @return string
	 */
	public function plugin_url() {
		return untrailingslashit( plugins_url( '/', __FILE__ ) );
	}

	/**
	 * Get the plugin path.
	 * @return string
	 */
	public function plugin_path() {
		return untrailingslashit( plugin_dir_path( __FILE__ ) );
	}

	/**
	 * Get the plugin path.
	 * @return string
	 */
	public function get_wc_version() {
		if (defined('WC_VERSION')) {
			$wc_version = WC_VERSION;
		} elseif (defined('WOOCOMMERCE_VERSION')) {
			$wc_version = WOOCOMMERCE_VERSION;
		} else {
			$wc_version = '1.0';
		}
		return $wc_version;
	}

	/**
	 * Declare Woo features compatibility.
	 */
	public function woo_features_compatibility() {
		if ( class_exists( \Automattic\WooCommerce\Utilities\FeaturesUtil::class ) ) {
			// HPOS (compatible)
			\Automattic\WooCommerce\Utilities\FeaturesUtil::declare_compatibility( 'custom_order_tables', __FILE__, true );
			
			// Cart & Checkout Blocks (incompatible)
			\Automattic\WooCommerce\Utilities\FeaturesUtil::declare_compatibility( 'cart_checkout_blocks', __FILE__, false );
		}
	}

} // class WPO_WC_Postcode_Checker

endif; // class_exists

/**
 * Returns the main instance of WC to prevent the need to use globals.
 *
 * @return WPO_WC_Postcode_Checker|null
 * @since  2.1
 */
function WPO_WCNLPC() {
	return WPO_WC_Postcode_Checker::instance();
}

WPO_WCNLPC(); // load plugin